export interface Post {
    id: number
    title: string
    date: number
    frontImage: string
    style: string
}
