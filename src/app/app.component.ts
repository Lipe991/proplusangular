import { Component,
  OnDestroy,
  OnInit } from '@angular/core';
import scrollIntoView from 'smooth-scroll-into-view-if-needed'
import { ApiService } from './api/api.service'
import { Post } from './model/post'
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  currentElement?: HTMLElement
  posts: Array<Post> = []
  postObserver: Subscription

  constructor( private httpModule: ApiService ) {}

  public ngOnInit(): void {
    this.postObserver = this.httpModule
    .getPostList()
    .subscribe(results => {
      this.posts = results
    })

    setInterval(() => {
      this.intervalCallback()
    },5000)
  }

  public ngOnDestroy(): void {
    this.postObserver.unsubscribe();
  }

  private intervalCallback() {
    if( this.currentElement = this.getNextView() ) {
      scrollIntoView(this.currentElement, { behavior: 'smooth' })
    } else {
      this.currentElement = <HTMLScriptElement>this.getScroller().firstChild
      scrollIntoView(this.currentElement, { behavior: 'instant' })
    }
  }

  private getScroller(): HTMLElement {
    return document.getElementById("scroll")
  }

  private getNextView(): HTMLElement {
    return this.currentElement == null ? <HTMLScriptElement>this.getScroller().childNodes[1] : <HTMLScriptElement>this.currentElement.nextSibling
  }

}
