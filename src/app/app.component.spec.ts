import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { FullscreenimagePipe } from './fullscreenimage.pipe';


describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

});
