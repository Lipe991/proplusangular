import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fullScreenImage'
})
export class FullscreenimagePipe implements PipeTransform {

  transform(value: string): string {
    const width = document.documentElement.clientWidth
    const height = document.documentElement.clientHeight
    return value.replace("PLACEHOLDER",width+"x"+height)
  }

}
