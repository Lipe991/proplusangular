import { FullscreenimagePipe } from './fullscreenimage.pipe';

describe('FullscreenimagePipe', () => {
  it('create an instance', () => {
    const pipe = new FullscreenimagePipe();
    expect(pipe).toBeTruthy();
  });

  it('should return correct screen dimension in url', () => {
    const pipe = new FullscreenimagePipe();
    const width = document.documentElement.clientWidth
    const height = document.documentElement.clientHeight
    const url = "https://images.24ur.com/media/images/PLACEHOLDER/May2018/ca0488e5e4_62084862.jpg?v=d41d"
    
    expect(pipe.transform(url)).toBe("https://images.24ur.com/media/images/"+width+"x"+height+"/May2018/ca0488e5e4_62084862.jpg?v=d41d")
  })
});
