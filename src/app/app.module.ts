import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ApiModule } from './api/api.module'
import { AppComponent } from './app.component';
import { FullscreenimagePipe } from './pipes/fullscreenimage.pipe';

@NgModule({
  declarations: [
    AppComponent,
    FullscreenimagePipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [ApiModule]
})
export class AppModule { }
