import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, Subscriber } from 'rxjs'
import { Post } from '../model/post'
import 'rxjs/add/operator/map';
import { ApiConfig } from '../config'

@Injectable()
export class ApiService {

  public constructor(private httpClient: HttpClient) {}

  public getPostList(): Observable<[Post]> {
    return this
      .httpClient
      .get<any>(ApiConfig.url,{headers: ApiConfig.headers})
      .map(result => {
        return result.data.front.articles.map(r => { 
          return this.jsonToPost(r)
        })
      })
  }

  jsonToPost(object: any): Post {
    return {
      id: object.id,
      title: object.title,
      date: object.date,
      frontImage: object.frontImage.src,
      style: object.style
    }
  }
}
